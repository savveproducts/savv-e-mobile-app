(function () {
    /**
     * @author Sundar
     * @package ms.app
     * @version 1.0.0
     * @since 1.0.0
     * @copyright  Copyright (c) 2014 Savv-e Pty Ltd
     Controllers in angular js are defined like so. Every controller has an associated call back which feed in the dependencies
     Also the $scope variable defines the scope of the controller.
     */
    window.ms.app.controller = {};

    window.ms.utilities = window.ms.utilities || {};
    window.ms.utilities.getProperty = function getProperty(propertyName, object) {
        var parts = propertyName.split("."),
            length = parts.length,
            i,
            property = object || this;

        for (i = 0; i < length; i++) {
            property = property[parts[i]];
        }

        return property;
    }

    /**
     * Core application controller
     * @param $scope
     * @param $location
     * @param $state
     * @param $ionicModal
     * @param $rootScope
     * @param loginTracker
     * @param elementList
     * @param pageVars
     */
    window.ms.app.controller.appCtrl = function ($scope, $location, $state, $ionicModal, $rootScope, loginTracker, elementList, pageVars , pageVariable) {

        /* Define all callbacks */
        var unregisterPageWatch,

            loginObserverCallback = function loginObserverCallback(isLoggedIn) {
                $scope.isLoggedIn = loginTracker.getIsLoggedIn() || false;
                if ($scope.isLoggedIn == true) {
                    $state.go('dashboard');
                }
                else {
                    $state.go('home.welcome');
                }
            },
            elementObserverCallback = function elementObserverCallback(elements) {
                //set up the elements. These elements will be used by the viewfiles/directives
                $scope.elements = elements;
            },
            pagevarsObserverCallback = function elementObserverCallback(pagevars) {
                //set up the elements. These elements will be used by the viewfiles/directives
                if (unregisterPageWatch !== undefined) {
                    unregisterPageWatch();
                }

                pageVariable.setVariable(pagevars['pagevars'],'pagevars');

                $scope.pageVars = pagevars['pagevars'];
                /** These global variables are directly referenced using the getGlobal function **/
                //Generic defaults. This is available across the entire site
                window.ms.app.globalVars = $scope.getVar('globalVars');
                $scope.getGlobal = function(varname, defaultValue){
                    defaultValue  = defaultValue || "";
                    return window.ms.app.globalVars[varname]||defaultValue;
                }

            };

        //This is the list of all page vars mentioned by the pagevars.json
        $scope.pageVars = null;
        $scope.$state = $state;


        //Get a variable from the current scope
        /**
         * @param varname name to be matched to
         * @param varscope Scope can be current , target or parent
         * @returns {*}
         */
        $scope.getVar = function (varname, varscope) {
            if (!$scope.pageVars) return "";
            varscope = varscope || 'target';

            var retVar = '';
            try {
                if (varscope == 'current') {
                    //get a variable defined in the scope.
                    //for example page.title in home.welcome will return home.welcome.page.title if defined or return an empty string
                    var state = $scope.$state.current.name;
                    //get a property mentioned in the view file
                    retVar =  window.ms.utilities.getProperty((state + '.' + varname), $scope.pageVars);
                }
                else if(varscope == 'parent'){
                    var states = $scope.$state.current.name.split('.');
                    states.pop();
                    states.push(varname);
                    //get a property mentioned in the view file
                    retVar =  window.ms.utilities.getProperty(states.join('.'), $scope.pageVars);
                }
                else if(varscope == 'target'){
                    //Get any of the variables. (Used if you are sharing the property across multiple pages
                    //to use it you need the whole path. eg: home.welcome.page.title
                    retVar =  window.ms.utilities.getProperty(varname, $scope.pageVars);
                }
            }
            catch (e) {
                console.log(e.message);
                return "";
            }

            if(typeof retVar == 'object' && retVar.value !== undefined){
                return retVar.value;
            }

            //its a string or an array. If undefined be nice and return an empty string
            return retVar || "";
        };

        unregisterPageWatch = $scope.$watch($scope.pageVars, function () {
        });


        //Initialize login observer change hte pages on user login;
        loginObserverCallback(loginTracker.getIsLoggedIn());
        loginTracker.getLocalObserver().registerObserverCallback(loginObserverCallback);

        //Initialize the element list loader
        elementList.setElementFile('data/elements.json');
        elementList.setKey('elements');
        elementList.getLocalObserver().registerObserverCallback(elementObserverCallback);
        elementList.loadElementFile();


        pageVars.setElementFile('data/pagevars.json');
        pageVars.getLocalObserver().registerObserverCallback(pagevarsObserverCallback);
        pageVars.loadElementFile();


        // Create a login Modal for use later
        $ionicModal.fromTemplateUrl('templates/login.html', {scope: $scope})
            .then(function (modal) {
                $scope.loginModal = modal;
            });

        /*
         Show the login modal we created in the previous step.
         This is accessed from the view
         */
        $scope.showLogin = function () {
            $scope.loginModal.show();
        }

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.loginModal.hide();
        };



    }


    /**
     * Controller for the home page
     * @param $scope
     * @param $location
     * @param $state
     * @constructor
     */
    window.ms.app.controller.HomeCtrl = function ($scope, $location, $state) {

    };


    window.ms.app.controller.AboutCtrl = function($scope,$location,$state){
        $scope.searchFilter = {column : 'all',searchfield  : 'all columns'};
        $scope.changeFiltering = function(filter){
            var scopeFilter = $scope.searchFilter;
            if($scope.searchFilter !== filter){
                scopeFilter.column = filter;
                if(filter == 'all'){
                    scopeFilter.searchfield = 'all columns';
                }
                else{
                    scopeFilter.searchfield = filter+' column';
                }
                $scope.searchFilter = scopeFilter;
            }
        };

        $scope.sort = {
            column: '',
            descending: false
        };
        $scope.changeSorting = function(column) {
            var sort = $scope.sort;
            if (sort.column == column) {
                sort.descending = !sort.descending;
            } else {
                sort.column = column;
                sort.descending = false;
            }


        };

    }


    window.ms.app.controller.EmployeeCtrl = function($scope,$location,$state,$rootScope,$filter,$stateParams,pageVariable){
        var pagevars = pageVariable.getVariable('home'),users;

        $scope.pagevars = pagevars;

        users = (function(){
                var users,
                    getOneById = function(id){
                        var found = $filter('filter')(users,{id:parseInt(id)},true);
                        if (found.length) {
                            return found[0];
                        }
                        return null;
                    },
                    getAll = function(search){
                        var found = $filter('filter')(users,{$:search},true);
                        if (found.length) {
                            return found;
                        }
                        return null;
                    }

                try{
                    users = pagevars['about']['users'];
                }
                catch(e){
                    users = [];
                }


                return {
                    users:users,
                    getOneById:getOneById,
                    getAll:getAll
                };
            })();

            $scope.employee = users.getOneById($stateParams.id);
    }
    /**
     * Now create an angular module for the controller .
     */
    angular.module('ms.app.controller',
            [
                //List all external modules being included
                'ionic',
                //List any custom modules being built by us
                'ms.app.service',
            ])

        .controller('AppCtrl', ['$scope', '$location', '$state', '$ionicModal','$rootScope', 'loginTracker', 'elementList', 'pageVars', 'pageVariable', window.ms.app.controller.appCtrl])
        .controller('HomeCtrl', ['$scope', '$location', '$state', window.ms.app.controller.HomeCtrl])
        .controller('AboutCtrl',['$scope', '$location', '$state', window.ms.app.controller.AboutCtrl])
        .controller('EmployeeCtrl',['$scope', '$location', '$state','$rootScope','$filter','$stateParams','pageVariable', window.ms.app.controller.EmployeeCtrl]);

})();
