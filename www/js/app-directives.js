/**
 * @author Sundar
 * @package ms.app
 * @version 1.0.0
 * @since 1.0.0
 * @copyright  Copyright (c) 2014 Savv-e Pty Ltd
 * Directives At a high level, directives are markers on a DOM element
 * @link :https://docs.angularjs.org/guide/directive
 */
window.ms.app.directives = {};


/**
 * Now create an angular module for the controller .
 */
angular.module('ms.app.directive', [
        'ms.app.service'
    ])
    .directive('msElement', ['$compile', '$http', '$templateCache', function ($compile, $http, $templateCache) {
        return {
            restrict: 'AEC',//A matches attribute , E matches element name , C matches class name
            template: '<div ng-include="getContentUrl()"></div>',
            link: function (scope, element, attrs) {

                /* Get the content url . This will map the right template to the directive */
                scope.getContentUrl = function () {
                    //Access the controller scope
                    var elementsList = scope.$parent['elements']
                        , listedElement;

                    //Make sure the controller has already loaded the elements
                    if (elementsList === undefined || elementsList.length == 0) {
                        return null;
                    }

                    listedElement = elementsList[attrs.handle];
                    if (listedElement === undefined || !listedElement || listedElement.templateUrl === undefined) {
                        return null;
                    }

                    return listedElement.templateUrl;
                }
                return true;
            }
        };
    }])

    /** Directive for displaying the online offline status **/
    .directive('onlineStatus', ['onlineOffline', function(onlineOffline) {

        return {
            restrict: 'AEC',
            'link': function(scope, element, attrs) {
                var onOnlineStatusChange = function onOnlineStatusChange(isOnline){
                    scope.status = isOnline;
                };

                onlineOffline.getLocalObserver().registerObserverCallback(onOnlineStatusChange);
                onOnlineStatusChange(onlineOffline.isOnline());
            },
            template:'<div ng-class="{true: \'icon balanced ion-information-circled\', false: \'icon assertive ion-close-circled\'}[status]">{{status==true?" online":" offline"}}</div>'
        };
    }])

