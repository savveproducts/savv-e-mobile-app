/**
 * @author Sundar
 * @package ms.app
 * @version 1.0.0
 * @since 1.0.0
 * @copyright  Copyright (c) 2014 Savv-e Pty Ltd

 ms.app is the global scope for our mobile project. All controllers / services etc will be in this score
 Controllers define  and manage the scope variables and action in angular js
 Here we define a module called ms.app and inject the controllers and the ionic object as dependencies
 Angular will look for a predefined object mentioned in the dependency and if not found try to create it using a factory
 In case it cannot create the dependency then an error will be generated.
 */

/**
 * This object namespace will contain any object definitions we have across the module
 * @type {{app: {}}}
 */
window.ms= {app:{}};

angular
    .module('ms.app',
        [
            //List all external modules being included
            'ionic',
            'LocalStorageModule',

            //List any custom modules being built by us
            'ms.app.router',
            'ms.app.controller',
            'ms.app.directive'
            //'ms.app.service',
        ])

    .config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        //All cookies/local storage will have this prefix now
        localStorageServiceProvider.setPrefix('ms.app');
    }])

    .run(function ($ionicPlatform) {
        //Initialize the ionic platform.
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    });