/**
 * @author Sundar
 * @package ms.app
 * @version 1.0.0
 * @since 1.0.0
 * @copyright  Copyright (c) 2014 Savv-e Pty Ltd
 Routers form the basic routing for the angular js application.
 When an url is entered the angular js splits the url and maps it to the right controller.
 The controller also receives any parameters mentioned as part of the route
 Here we are defining any routing algorithm for the ms.app
 In ionic we use the ui-router and manage routes using states
 @link : https://github.com/angular-ui/ui-router
 */


angular.module('ms.app.router',
        [
            'ui.router'
        ])
    .config(function ($stateProvider, $urlRouterProvider) {
        //if the states dont match go to the home page

        $stateProvider
            .state('home', {
                url: "/home",
                abstract: true,
                templateUrl: "templates/home.html",
                controller: "HomeCtrl"
            })
            .state('home.welcome', {
                url: "/welcome",
                views: {
                    'welcome-tab': {
                        templateUrl: "templates/partials/home/welcome.html"
                    }
                }
            })
            .state("home.info", {
                url: "/info",
                views: {
                    'welcome-tab': {
                        templateUrl: "templates/partials/home/info.html"
                    }
                }
            })
            .state("home.aboutus", {
                url: "/about",
                views: {
                    'about-tab': {
                        templateUrl: "templates/partials/home/about.html"
                    }
                }
            })
            .state("home.employee", {
                url: "/employee/:id",
                views: {
                    'about-tab': {
                        templateUrl: "templates/partials/home/employee.html",
                        controller:'EmployeeCtrl'
                    }
                }
            })
            .state("home.contact", {
                url: "/contact",
                views: {
                    'contact-tab': {
                        templateUrl: "templates/partials/home/contact.html"
                    }
                }
            })
            .state("login", {
                url: "/login",
                templateUrl: "templates/login.html"
            })
            .state("logout", {
                url: '/logout',
                templateUrl: "templates/logout.html"
            })
            .state("dashboard", {
                name: 'dashboard',
                url: '/dashboard',
                templateUrl: "templates/dashboard.html"
            });

        $urlRouterProvider.otherwise("/home/welcome");
    });

