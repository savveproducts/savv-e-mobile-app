/**
 * @author Sundar
 * @package ms.app
 * @version 1.0.0
 * @since 1.0.0
 * @copyright  Copyright (c) 2014 Savv-e Pty Ltd
 * Services can be factories (that generate something) or a service which is predefined and has a inbuilt constructor
 */

(function (window) {
    window.ms.app.service = {};

    /**
     * The observer call back is a way of manually tracking a variable.
     * The $watch has a massive overhead. By manually notifying the observers we can avoid that
     * @returns {{registerObserverCallback: Function, notifyObservers: Function}}
     */
    window.ms.app.service.observerCallback = function () {
        var observerCallbacks = []
            , registerObserverCallback = function (callback) {
                //register an observer
                observerCallbacks.push(callback);
            }
            , notifyObservers = function () {

                var args = [].splice.call(arguments, 0);
                //Notify the observer with what we want it to know. Also pass in all the variables
                angular.forEach(observerCallbacks, function (callback) {
                    callback.apply(this, args);
                });
            };

        return {
            'registerObserverCallback': registerObserverCallback,
            'notifyObservers': notifyObservers,
            'length': (function () {
                return observerCallbacks.length;
            }())
        };
    }

    /**
     * Watches for the user to log in or log out and responses correspondingly
     * @param observerCallback
     * @param localStorageService
     * @returns {{getLocalObserver: Function, getIsLoggedIn: Function, login: Function, logout: Function}}
     */
    window.ms.app.service.loginTracker = function (localStorageService) {


        var observerCallback = new window.ms.app.service.observerCallback()
        /* Get the local observer to manage callbacks */
            , getLocalObserver = function getLocalObserver() {
                return observerCallback;
            }
            , isLoggedIn = localStorageService.get('isLoggedIn') || false
        /* Check if the user is logged in */
            , getIsLoggedIn = function getIsLoggedIn() {
                isLoggedIn = localStorageService.get('isLoggedIn') || false;
                return isLoggedIn;
            }
        /* set the login status of the user */
            , setIsLoggedIn = function setIsLoggedIn(value) {
                isLoggedIn = value || false;
                localStorageService.set('isLoggedIn', isLoggedIn);
                getLocalObserver().notifyObservers();
                return this;
            }
        /* set the login status of the user */
            , login = function login() {
                return setIsLoggedIn(true);
            }
            , logout = function login() {
                return setIsLoggedIn(false);
            }

        //manage the initial status of the learner
        setIsLoggedIn(isLoggedIn);


        /**
         * Only expose the functions you want to expose. Others automatically become private functions
         * eg:setIsLoggedIn is a private function. The only way of accessing it is through login or logout
         * So this keeps the code clean and also gives us a one point management for future additions
         * This is true with variables as well.
         */
        return {
            'getLocalObserver': getLocalObserver,
            'getIsLoggedIn': getIsLoggedIn,
            'login': login,
            'logout': logout
        }
    };


    /**
     * @todo replace $http with $resource
     * Load the element list
     * @param $http
     */
    window.ms.app.service.fileLoader = function fileLoader($http) {
        var observerCallback = new window.ms.app.service.observerCallback()
        /* Get the local observer to manage callbacks */
            , getLocalObserver = function getLocalObserver() {
                return observerCallback;
            },
            key = null,
            elementFile = 'data/elements.json',
            lastLoadedFile = null,
            allElements = [],
            setKey = function setKey(k) {
                key = k;
            },
            setElementFile = function setElementFile(file) {
                elementFile = file;
            },
            getElementFile = function getElementFile() {
                return elementFile;
            },
            loadElementFile = function loadElementFile(notify) {
                if (lastLoadedFile == elementFile) {
                    return false;
                }
                notify = notify || true;
                //Load the element file and on completion notify the watching observers
                $http.get(getElementFile()).success(function (data) {
                    if (key !== undefined && key !== null) {
                        allElements = data[key];
                    }
                    else {
                        allElements = data;
                    }
                    if (notify == true) {
                        getLocalObserver().notifyObservers(allElements);
                    }
                });
            },
            getAllElements = function getAllElements() {
                return allElements;
            };


        return {
            'setElementFile': setElementFile,
            'getElementFile': getElementFile,
            'loadElementFile': loadElementFile,
            'getAllElements': getAllElements,
            'getLocalObserver': getLocalObserver,
            'setKey': setKey
        }
    }


    /**
     * Service for tracking when the user goes online or offline
     * @param $window
     * @param $rootScope
     * @returns {{}}
     */
    window.ms.app.service.onlineOffline = function ($window, $rootScope) {
        var observerCallback = new window.ms.app.service.observerCallback()
        /* Get the local observer to manage callbacks */
            , onlineStatus = {};

        onlineStatus.getLocalObserver = function getLocalObserver() {
            return observerCallback;
        };

        onlineStatus.onLine = $window.navigator.onLine;

        onlineStatus.isOnline = function () {
            return onlineStatus.onLine;
        }

        onlineStatus.changeStatus = function (isOnline) {
            alert('change status '+isOnline);
            onlineStatus.onLine = isOnline;
            $rootScope.$digest();
            onlineStatus.getLocalObserver().notifyObservers(onlineStatus.onLine);
        }


        if ($window.addEventListener) {
            $window.addEventListener("online", function () {

                onlineStatus.changeStatus(true);
            }, true);

            $window.addEventListener("offline", function () {
                onlineStatus.changeStatus(false);
            }, true);
        }
        else {
            document.body.ononline = function () {
                onlineStatus.changeStatus(true);
            }

            document.body.onoffline = function () {
                onlineStatus.changeStatus(false);
            }
        }

        return onlineStatus;
    }


    window.ms.app.service.pageVariable = function(){
        var observerCallback = new window.ms.app.service.observerCallback()
        /* Get the local observer to manage callbacks */
            ,pageVariables  = {};

        pageVariables.getLocalObserver = function getLocalObserver() {
            return observerCallback;
        };

        pageVariables.getVariable = function(key){
            key = key || null;
            if(!key) return pageVariables;
            return pageVariables[key];
        }


        pageVariables.setVariable = function(value,key){
            key = key || null;
            if(!key) return pageVariables[key] = value;
            pageVariables = value;
        }
        return pageVariables;

    };
    /**
     * Now create an angular module for the service .
     */
    angular.module('ms.app.service',
            [
                'LocalStorageModule'
            ])
        .factory('observerCallback', window.ms.app.service.observerCallback)
        .factory('onlineOffline', ["$window", "$rootScope", window.ms.app.service.onlineOffline])
        .factory('loginTracker', ['localStorageService', window.ms.app.service.loginTracker])
        .factory('elementList', ['$http', window.ms.app.service.fileLoader])
        .factory('pageVars', ['$http', window.ms.app.service.fileLoader])
        .factory('pageVariable', [window.ms.app.service.pageVariable]);
}(window));
