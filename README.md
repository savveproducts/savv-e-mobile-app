# Savv-e mobile framework.

## Product Concept: 
This product will be based off ionic / angular js .

## Getting Started
To get started, simply clone this repository .

##SAVV-E standards for writing js for mobiles##
I just started putting a few things down . This is available in the documentation folder.
Here are the links
* [File structure](documentation/file-structure.md)
* [Generic coding standards](documentation/generic-angular-coding.md)



### Prerequisites
### Must have
* Cordova 
* Ionic
* Apache Ant
* Set up JAVA_HOME ,  ANT_HOME 
* For developing android set up ADT /Eclipse plugin


### Good to have
* Git [http://git-scm.com/](http://git-scm.com/).
* NodeJs [http://nodejs.org/](http://nodejs.org/)..
* Bower installed globally
* Grunt installed globally
* Add npm to your path (for bower to work properly). *(http://stackoverflow.com/questions/12369390/bower-command-not-found)

### Configuration
* npm is configured to run bower and gulp upon installation.  So 
```
npm install

```
This will install all dependencies and call bower install.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the angular framework files


### Set up an ionic project (Only do this if you are starting from scratch)

```bash
$ sudo npm install -g ionic
```

Then run:

```bash
$ sudo npm install -g ionic
$ ionic start myProject tabs
```
While we recommend using the `ionic` utility to create new Ionic projects, you can use this repo as a barebones starting point to your next Ionic app.

To use this project as is, first clone the repo from GitHub, then run:

```bash
$ cd ionic-app-base
$ sudo npm install -g cordova ionic gulp
$ npm install
```




### For production
```
npm start
```
Now browse to the app at `http://localhost:8000/app/index.html`.
(Instantiates the local server)

## Testing
There are two kinds of tests in the angular-seed application: Unit tests and End to End tests.

### Running Unit Tests
>[For running tests](http://jasmine.github.io/1.3/introduction.html)
Unit tests are written in jasmine and run using Karma. 
* the configuration is found at `test/karma.conf.js`
* the unit tests are found in `test/unit/`.

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```
or
```
npm run test-single-run
```

## Using Sass (optional)

This project makes it easy to use Sass (the SCSS syntax) in your projects. This enables you to override styles from Ionic, and benefit from
Sass's great features.

Just update the `./scss/ionic.app.scss` file, and run `gulp` or `gulp watch` to rebuild the CSS files for Ionic.

Note: if you choose to use the Sass method, make sure to remove the included `ionic.css` file in `index.html`, and then uncomment
the include to your `ionic.app.css` file which now contains all your Sass code and Ionic itself:

```html
<!-- IF using Sass (run gulp sass first), then remove the CSS include above
<link href="css/ionic.app.css" rel="stylesheet">
-->
```

### COMPILING SASS
Node in the project is also configured to compile sass using gulp. To utilize this please run
```
npm run-script compile
```
This will compile the sass file in the www folder and place the output css in the css folder




### End to end testing

* the configuration is found at `test/protractor-conf.js`
* the end-to-end tests are found in `test/e2e/`

Protractor simulates interaction with our web app and verifies that the application responds
correctly. Therefore, our web server needs to be serving up the application, so that Protractor
can interact with it.

```
npm start
```

In addition, since Protractor is built upon WebDriver we need to install this.  The angular-seed
project comes with a predefined script to do this:

```
npm run update-webdriver
```

This will download and install the latest version of the stand-alone WebDriver tool.Then
```
npm run protractor
```

## Updating 

You can update the tool dependencies by running:

```
npm update
```

This will find the latest versions that match the version ranges specified in the `package.json` file.

You can update the Angular dependencies by running:

```
bower update
```

This will find the latest versions that match the version ranges specified in the `bower.json` file.




### Running the App in Production

**Only upload the app directory into production **

If your Angular app is talking to the backend server via xhr or other means, you need to figure
out what is the best way to host the static files to comply with the same origin policy if
applicable. Usually this is done by hosting the files by the backend server or through
reverse-proxying the backend server(s) and webserver(s).


## Continuous Integration

Integrate either Travis CI or Jenkins CI at a later stage