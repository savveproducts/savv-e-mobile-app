## GENERIC ANGULAR ##
###FILE ORGANIZATION###
Refer to this [link]()


### REFERENCE ###
[Angular Tutorials](https://docs.angularjs.org/tutorial/step_00) can be found here. Please take some time to read through to get an understanding.
[Advanced Javascript Tutorials](http://chimera.labs.oreilly.com/books/1234000000262/ch01.html) can be found here. This
is a very good tutorial for understanding javascript and why we do things a certain way.


### STANDARDS ###
* Use {{Angular expressions }} for any ad-hoc expressions. This will run the expressions in the current model scope instead of global scope.
* Name all variables / controllers / directives etc as understandable and readable names that points to their purpose
    > For eg: IndexController will reference the indexAction or the default action etc....
* Do not name variables with a $ prefix. Angular uses that for namespacing and so might cause conflicts
* Annotate variables used in dependency to avoid problems when minifying. ([See Here](https://docs.angularjs.org/tutorial/step_05))

### DIRECTIVES ###
* Make sure all business logic goes into directives. This gives us better usability and reuse of code.
* Name of the directives needs to match the html code. For eg: The directive ngApp is represented by the code
  ```
  <html ng-app>
  ```


### VIEWS ###
* Try avoiding hard coding in views. One of the potential future requirement is to add the facility to have a CMS.
  To provide this feature we will need the ability to update the content through a JSON file or something similiar.Use templates as much as possible.

