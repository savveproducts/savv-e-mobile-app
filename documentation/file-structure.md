## File structure for angular js file (Current as of 1.0.0) ##
* node_modules ( All modules installed through npm )
* platforms (For cordova compilation. Do not change anything here)
* hooks (internal use only)
* plugins ( For cordova use only)
* scss (Any sass files. By adding a watch you can autocompile things or manually run the following code to compile sass using gulp)

    ```
    npm compile
    ```

* www (All html  / js files that needs to be compiled by cordova or used in the application)

Note on organizing javascript files. Google's guide for  managing file applications can be [found here](http://google-styleguide.googlecode.com/svn/trunk/angularjs-google-style.html)
Here is the organization [recommended by google](https://docs.google.com/document/d/1XXMvReO8-Awi1EZXAXS4PzDzdNvV6pGcuaF4Q9821Es/pub)
To give it a bit more structure and match it with the current standards we follow . we will have the css in the css folder and the js will be the root for the app

```
---
sampleapp/
        index.html
        Readme.md : Update this with any information you want logged.Please visit [here for the documentation syntax](http://daringfireball.net/projects/markdown/syntax)
        css
            app.css
            Any compiled css from sass
        img
            Any assets goes here
        lib
            All bower modules are installed here. This does not get versioned. Do not put any files here. it will be reset.
        templates
            All view files goes here
        js
            app.js                                top-level configuration, route def’ns for the app
            app-controller.js
            app-controller_test.js
            components/
                adminlogin/
                        adminlogin.css                styles only used by this component
                        adminlogin.js              optional file for module definition
                        adminlogin-directive.js
                        adminlogin-directive_test.js
                        private-export-filter/
                                private-export-filter.js
                                private-export-filter_test.js
                userlogin/
                        somefilter.js
                        somefilter_test.js
                        userlogin.js
                        userlogin.css
                        userlogin.html
                        userlogin-directive.js
                        userlogin-directive_test.js
                        userlogin-service.js
                        userlogin-service_test.js
                        etc...
---
```

