var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var paths = {
  sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', ['sass-check'],function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check','cordova-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});


gulp.task('git-check', function(done) {

  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  else{
      console.log(' '+gutil.colors.green('Git is installed and has been located successfully.'))
  }
  done();
});

gulp.task('cordova-check', function(done) {

  if (!sh.which('cordova')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Cordova is the base of this application and is required to run this application.',
      '\n  Download cordova here:', gutil.colors.cyan('http://cordova.apache.org/') + '.',
      '\n  Once cordova is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  else{
      console.log(' '+gutil.colors.green('Cordova is installed and has been located successfully.'))
  }
  done();
});

gulp.task('sass-check', function(done) {

  if (!sh.which('sass')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Sass could make building of css so much faster,.',
      '\n  Visit:', gutil.colors.cyan('http://sass-lang.com//') + ' to gain an understanding of SASS.',
      '\n  Once sass is installed, run \'' + gutil.colors.cyan('npm run-script compile') + '\' again.'
    );
    process.exit(1);
  }
  else{
      console.log(' '+gutil.colors.green('Sass is located. Compiling SASS....'))
  }
  done();
});
